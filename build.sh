#!/bin/bash

# Build a multi-platform docker image for stvid
# Un-comment respective lines to push the built images to a registry (not tested yet)

# CI_REGISTRY="registry.gitlab.com"
# CI_REGISTRY_USER=""
# CI_REGISTRY_PASSWORD=""
# PUSH=""

IMAGE_NAME="stvid"
IMAGE_TAG="main"
VERSION="dev-20230421"

#CI_REGISTRY_IMAGE="registry.gitlab.com/kerel-fs/docker-stvid"
CI_REGISTRY_IMAGE=""
CI_COMMIT_SHA="$(git rev-parse HEAD)"

GITLAB_CI_IMAGE_DOCKER='docker:20.10.18'
GITLAB_CI_IMAGE_BINFMT='tonistiigi/binfmt:qemu-v7.0.0-28'
GITLAB_CI_DOCKER_BUILDX_PLATFORMS='linux/amd64' # linux/arm/v7,linux/arm64'

export VERSION
export CI_REGISTRY_IMAGE
export IMAGE_NAME

# # NOTE: Run only once
# #############################
# docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --uninstall qemu-*
# docker run --privileged --rm ${GITLAB_CI_IMAGE_BINFMT} --install all
# 
# # docker buildx rm container
# docker buildx \
#   create \
#   --use \
#   --name container \
#   --driver docker-container \
#   --bootstrap \
# #############################

# docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker buildx bake \
  --progress plain \
  -f docker-compose.ci.yml \
  --pull \
  --set "*.platform=$GITLAB_CI_DOCKER_BUILDX_PLATFORMS" \
  --set "*.tags=$IMAGE_NAME:$IMAGE_TAG" \
  --set "*.args.VCS_REF=$CI_COMMIT_SHA" \
  --load
#  --print

#  ${PUSH:+ --set "*.tags=$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG"} \
#  ${PUSH:+ --push} \
#
#  --push \
#  $IMAGE_NAME
