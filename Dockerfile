# STVID container image
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
# SPDX-License-Identifier: AGPL-3.0

FROM debian:bullseye

ARG TARGETPLATFORM
ARG VCS_REF
ARG VERSION

ARG STVID_UID=999
ARG STVID_VARSTATEDIR=/var/lib/stvid
ARG STVID_SRCDIR=/usr/local/src/stvid

LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'
LABEL org.opencontainers.image.revision="${VCS_REF}"

# Ensure python output appears swiftly in logs
ENV PYTHONUNBUFFERED 1

ENV PATH="$PATH:/usr/local/src/docker-stvid/bin"

# Install system packages
COPY packages.debian /usr/local/src/docker-stvid/
COPY packages-astrometry.debian /usr/local/src/docker-stvid/
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    apt-get update \
    && xargs -a /usr/local/src/docker-stvid/packages.debian apt-get install -qy \
    && xargs -a /usr/local/src/docker-stvid/packages-astrometry.debian apt-get install -qy \
    && rm -r /var/lib/apt/lists/*

# Workaround: re-add old command sextractor
RUN ln -s /usr/bin/source-extractor /usr/local/bin/sextractor

# Install python packages (which are unavailable via apt)
RUN pip3 install zwoasi~=0.1.0.1 picamerax~=21.9.8 spacetrack~=0.16.0 --prefer-binary

# Install hough3d-code
RUN git clone https://gitlab.com/pierros/hough3d-code.git /hough3d-code &&\
    cd /hough3d-code &&\
    make all &&\
    cp -p hough3dlines /usr/local/bin/hough3dlines &&\
    cd / && rm -rf /hough3d-code

# Install satpredict
RUN git clone https://github.com/cbassa/satpredict /satpredict &&\
    cd /satpredict &&\
    make &&\
    make install &&\
    cd / && rm -rf /satpredict

# Install libasi
# Note: ZWOASI SDK Version 1.29 can be found in indi3rd-party v2.0.2
ARG INDI_3RDPARTY_VERSION=v2.0.2
RUN case ${TARGETPLATFORM} in \
     "linux/amd64")  ARCH=x64    ;; \
     "linux/arm64")  ARCH=armv8  ;; \
     "linux/arm/v7") ARCH=armv7  ;; \
     "linux/arm/v6") ARCH=armv6  ;; \
     "linux/386")    ARCH=x86    ;; \
     *) echo "Unknown platform: ${TARGETPLATFORM}"; exit 1 ;; \
    esac && \
    wget -q "https://raw.githubusercontent.com/indilib/indi-3rdparty/${INDI_3RDPARTY_VERSION}/libasi/${ARCH}/libASICamera2.bin" -O "/usr/local/lib/libASICamera2.so" && \
    chmod +x "/usr/local/lib/libASICamera2.so"

# Add unprivileged system user
RUN groupadd -r -g ${STVID_UID} stvid \
    && useradd -r -u ${STVID_UID} \
        -g stvid \
        -d ${STVID_VARSTATEDIR} \
        -s /sbin/nologin \
        -G audio,dialout,plugdev \
        stvid

# Create application varstate directory
RUN install -d -o ${STVID_UID} -g ${STVID_UID} ${STVID_VARSTATEDIR}

# Download source code
RUN git clone -c advice.detachedHead=false --depth 1 -b ${VERSION} https://github.com/kerel-fs/stvid.git ${STVID_SRCDIR}

# Create configuration directory
RUN mkdir /etc/stvid/

# Install scripts
COPY bin /usr/local/src/docker-stvid/bin

ENTRYPOINT ["/bin/bash", "-c"]
