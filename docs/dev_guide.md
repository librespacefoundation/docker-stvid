## Developer guide

The SatNOGS Optical Station Deployment Script creates local git checkouts of all relevant repositories.
These are bind-mounted into the respective containers to easily allow for modifications.

### Building

Run the following command:
```
./build.sh
```

This will build the image `stvid:main`. Both, the `stvid_acquire` and `stvid_process` service both use this image.

### Usage

Follow the [User Guide](./user_guide.md#Usage) now.

### Testing

TODO: Update this section for use with the multi-file stvid configuration.

1. Create stvid-config.ini based on example
docker-compose create
3. Copy obs from example dir:
   `docker cp examples/jebba-2022-09-04/obs docker-stvid-stvid_acquire-1:/var/lib/stvid/obs`
4. Copy tle from example dir:
   `docker cp examples/jebba-2022-09-04/tle docker-stvid-stvid_process-1:/var/lib/stvid/tle`
4. `docker-compose up stvid_process`
5. ``
