# Guide for SatNOGS Optical Station Setup (without using Ansible!)

## Installation

- Flash SD card with Raspbian
- Login to your Pi (e.g. via ssh)
- Install docker & docker-compose. On Debian/Raspian 12 (bookworm) run:
  ```
  sudo apt install docker.io apparmor docker-compose
  sudo adduser $(whoami) docker
  ```
- Re-login for the group permission to take effect.
- Run the following commands
  ```
  cd ~/
  wget https://gitlab.com/librespacefoundation/docker-stvid/-/raw/main/deploy/deploy_satnogs_optical.sh
  chmod +x deploy_satnogs_optical.sh
  ./deploy_satnogs_optical.sh
  ```

## Configuration

- cd into the created directory: `cd ~/station`
- Edit `stvid/user.ini` (to configure STVID)
- Edit `station.env` (to configure the satnogs-optical service)

STVID configuration is split in multiple files:
1. defaults.ini
2. paths.ini
3. user.ini

Values (re-)defined in later files overwrite previous settings.

## Test run stvid acquire

Run the following command:
```
docker-compose run stvid_acquire acquire --test
```

If there is an X server (or Xwayland) running on the same host or forwarded via ssh, to see a
live preview run:
```
docker-compose run -e DISPLAY=$DISPLAY stvid_acquire acquire --test --live
```

## Start the services

- run the following command
  `docker-compose up -d`

## Installation of docker & docker-compose

Install [docker](https://docs.docker.com/engine/install/) & [docker-compose](https://docs.docker.com/compose/install/).
If the installation of docker is not possible following the official documentation, you could follow
the steps of installation for Raspberry Pi 32-bit we documented in [this guide](docker_install_guide.md).
