[This guide is describing the installation of a third-party component (docker). Keeping such a guide up-to-date can't be guaranteed.
Whenever possible follow the official documentation for [Docker Engine Installation](https://docs.docker.com/engine/install/) instead.]

# Docker Installation on Raspberry Pi

Refer to docker installation on how to get the latest installed on your system.<br>
Short version, ymmv: Base image: Rasperry Pi OS 32bit Lite (bullseye):
```
# already installed: ca-certificates curl lsb-release
# optional: tmux uidmap
sudo apt update
sudo apt upgrade -y
sudo apt install -y ca-certificates curl gnupg lsb-release git

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker.gpg
echo "deb https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# add user to docker group, avoid needing sudo, re-login to apply
sudo adduser pi docker
```
