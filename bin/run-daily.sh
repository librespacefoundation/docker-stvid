#!/bin/sh

if [ "$1" = "" ]; then
    echo "What command do you want to run? For example, try \n\n  docker-compose run --rm stvid_acquire \"run-daily.sh acquire\""
    exit 1
fi
CMD="$1"

crontab <<EOF
0 12 * * * $CMD >/proc/1/fd/1 2>/proc/1/fd/2
EOF

$CMD >/proc/1/fd/1 2>/proc/1/fd/2 &

cron -f
