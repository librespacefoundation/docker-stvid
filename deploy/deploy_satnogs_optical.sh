#!/usr/bin/sh

set -e

## For installation on Raspberry Pi as root user
# STATION_DIR="/srv/stivd"
# SRC_DIR="/usr/local/src"
# STVID_CFG_DIR="/etc/stvid"
# INSTALL_UDEV_RULES=true

# For installation on desktop as non-root user
STATION_DIR="$(pwd)/station"
SRC_DIR="$STATION_DIR/src"
STVID_CFG_DIR="$STATION_DIR/stvid"
INSTALL_UDEV_RULES=false

# For local development (use cp instead of curl)
LOCAL_SRC=false

DOCKER_STVID_URL="https://gitlab.com/librespacefoundation/docker-stvid"
DOCKER_STVID_BRANCH=main

# Define helper functions
SCRIPT_PATH="$(dirname $0)"
download_file() {
    # Download the selected file from docker-stvid repo and save it to the selected output path
    if [ "$LOCAL_SRC" = true ]; then
        cp "$SCRIPT_PATH/$1" "$2"
    else
        BASE_URL="$DOCKER_STVID_URL/-/raw/$DOCKER_STVID_BRANCH/deploy"
        curl -sSL "$BASE_URL/$1" -o "$2"
    fi
}

# Create directory structure
echo "Create tle, obs and obs_archive in $STATION_DIR..."
mkdir -p "$STATION_DIR"
cd "$STATION_DIR"
mkdir "tle" "obs" "obs_archive"

# Clone source repositories
echo "Download source repositories"
mkdir -p "$SRC_DIR"
cd "$SRC_DIR"
export GIT_LFS_SKIP_SMUDGE=1 # Disable git-lfs
git clone "https://gitlab.com/kerel-fs/stvid.git"
git clone "https://gitlab.com/kerel-fs/satnogs-optical.git"
git clone --branch "$DOCKER_STVID_BRANCH" "$DOCKER_STVID_URL.git"

if [ "$INSTALL_UDEV_RULES" = true ]; then
    echo "Install udev rules"
    download_file "99-asi.rules" "/etc/udev/rules.d/99-asi.rules"
fi

# Create satnogs-optical config template
cd "$STATION_DIR"
cat <<EOF > "station.env"
# SatNOGS Credentials
SATNOGS_API_TOKEN = ''
SATNOGS_DB_API_TOKEN = ''
OBS_ARCHIVE_ENABLED = 'False'
EOF

# Create stvid config template
mkdir -p "$STVID_CFG_DIR"
cd "$STVID_CFG_DIR"
download_file "stvid_config.dist/defaults.ini" "defaults.ini"
download_file "stvid_config.dist/paths.ini" "paths.ini"
download_file "stvid_config.dist/user.ini" "user.ini"

cd "$STATION_DIR"
download_file "docker-compose.yml" "docker-compose.yml"

cat << EOF
# Next steps:
- Change into station directory
    cd $STATION_DIR
- Edit $STATION_DIR/station.env
- Edit $STVID_CFG_DIR/user.ini
- Test with:
    docker-compose run stvid_acquire acquire --test --live
- Start with:
    docker-compose up -d
- Happy satellite hunting!
EOF
